
const express = require('express');
const Contenedor =  require('./Contenedor')


let contenedor =   new Contenedor('productos.json');
let arr_products = contenedor.getAllsync();

let obj_productos={items:null,cantidad:0}
let obj_vistas={items:0,items_random:0}
const app = express()
const puerto=8077

const server= app.listen(puerto, ()=>{

    console.log("Iniciando servidor")
    // leo el archivo productos.json para poblar el array de productos 
    console.log("Recuperando el listado de productos del archivo: ")  
    console.log(JSON.stringify(arr_products));
    console.log( "servidor inicializado en puerto:" + server.address().port)

})

server.on("error", error=>console.error("error en el servidor : "+error))

app.get('/productos', (req , res)=> {
    res.json(arr_products)
} )

app.get('/productoRandom', (req , res)=> {
    //selecciono un item al azar del array de productos.
    let posicion= Math.floor(Math.random() * arr_products.length )
    //devuelvo
    res.json(arr_products[posicion])
} )




